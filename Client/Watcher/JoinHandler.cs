using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Smod2.API;
using Smod2.EventHandlers;
using Smod2.Events;
using UnityEngine;
using UnityEngine.Networking;

namespace Watcher
{
	internal class JoinEventHandler : IEventHandlerPlayerJoin
	{
		private readonly Watcher plugin;

		public JoinEventHandler(Watcher plugin) => this.plugin = plugin;

		public void OnPlayerJoin(PlayerJoinEvent ev)
		{
			// Connection check to make sure we don't send a notification after they are auto-disconnected.
			NetworkConnection connection = ((GameObject) ev.Player.GetGameObject()).GetComponent<CharacterClassManager>().connectionToClient;
			if (connection == null || !connection.isConnected) return;

			try
			{
				ReservedSlotCheck(ev.Player);
			}
			catch (Exception e)
			{
				plugin.Error("Failed to check reserved slot:\n" + e);
			}

			// To make sure global mods don't trigger the pings :P
			if (((GameObject)ev.Player.GetGameObject()).GetComponent<ServerRoles>().BypassStaff) return;
			try
			{
				NotifyWatchlists(ev.Player);
			}
			catch (Exception e)
			{
				plugin.Error("Failed to notify watchlists:\n" + e);
			}
		}

		private void ReservedSlotCheck(Player player)
		{
			const string reservedSlotComment = "Watcher - Studio Global Moderator";

			ReservedSlot[] slots = ReservedSlot.GetSlots();
			if (plugin.gmodSlot && ((GameObject) player.GetGameObject()).GetComponent<ServerRoles>().BypassStaff)
			{
				if (slots.All(x => x.SteamID != player.SteamId))
				{
					new ReservedSlot(player.IpAddress, player.SteamId, reservedSlotComment).AppendToFile();

					plugin.Info($"Added global moderator {player.Name} ({player.SteamId}) to the reserved slots.");
					player.PersonalBroadcast(4, "<size=30><color=#f82>You've been given an automatic reserved slot - Watcher</color></size>", false);
				}
				else
				{
					player.SendConsoleMessage("Welcome. You still have a Watcher reserved slot.");
				}
			}
			else if (slots.FirstOrDefault(x => x.SteamID == player.SteamId)?.Comment == reservedSlotComment)
			{
				plugin.Info($"Removed former global moderator {player.Name} ({player.SteamId}) from the reserved slots.");

				CustomNetworkManager cnm = (CustomNetworkManager) CustomNetworkManager.singleton;
				if (cnm.numPlayers > cnm.ReservedMaxPlayers) // Above max players, wouldn't have joined if they didn't have the reserved slot.
				{
					player.Disconnect("Your reserved slot due to being a global moderator was revoked.");
				}
			}
		}

		private void NotifyWatchlists(Player player)
		{
			WatcherMessageJson data = new WatcherMessageJson(plugin, player, plugin.Server);
			string jsonData = JsonConvert.SerializeObject(data);

			SendJoin(Watcher.kPrimaryServer, plugin.primaryRateLimiter, jsonData);
			foreach (KeyValuePair<string, RateLimiter> watchlist in plugin.rateLimiters)
			{
				SendJoin(watchlist.Key, watchlist.Value, jsonData);
			}
		}

		private static void SendJoin(string address, RateLimiter rateLimiter, string jsonData)
		{
			DownloadHandlerBuffer download = new DownloadHandlerBuffer();
			UploadHandlerRaw upload = new UploadHandlerRaw(Encoding.UTF8.GetBytes(jsonData));

			UnityWebRequest www = new UnityWebRequest(address, UnityWebRequest.kHttpVerbPOST, download, upload);
			www.SetRequestHeader("Content-Type", "application/json"); // Not really needed, but a nice courtesy

			rateLimiter.Enqueue(www);
		}
	}
}