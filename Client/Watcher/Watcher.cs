using System.Collections.Generic;
using Smod2;
using Smod2.Attributes;
using Smod2.Config;
using Smod2.Events;

namespace Watcher
{

	[PluginDetails(
		author = "Phoenix",
		name = "Watcher",
		description = "Watches your server for those bad bois",
		id = "Phoenix.Watcher",
		configPrefix = "w",
		version = "1.2",
		SmodMajor = 3,
		SmodMinor = 4,
		SmodRevision = 1
	)]
	public class Watcher : Plugin
	{
		public const string kPrimaryServer = "https://corruptionbot.xyz/watcher/watcher.php";

		[ConfigOption]
		public readonly bool disable = false;
		[ConfigOption]
		public readonly bool gmodSlot = true;
		//Automatically disconnects and bans users if/when requested by Watcher
		[ConfigOption]
		public readonly bool autoDisconnect = true;
		//Automatically bans that IP from the server, preventing them from joining again
		[ConfigOption]
		public readonly bool autoBan = true;
		[ConfigOption]
		public readonly string[] watchlists = new string[0];

		internal Dictionary<string, RateLimiter> rateLimiters;
		internal readonly RateLimiter primaryRateLimiter;

		public Watcher()
		{
			rateLimiters = new Dictionary<string, RateLimiter>();
			primaryRateLimiter = new RateLimiter(this, "primary");
		}

		public override void OnDisable() { } // SMod auto-logs disabling

		public override void OnEnable()
		{
			if (disable) PluginManager.DisablePlugin(this);
			// SMod auto-logs enabling
		}

		public override void Register()
		{
			// Registered as low so auto-disconnect (e.g. vpnshield) can run before this, preventing unnecessary pings.
			AddEventHandlers(new JoinEventHandler(this), Priority.Low);
		}
	}
}
